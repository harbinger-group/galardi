# Assignment

## _AWS Microservices For A Book Store_

Following assignment will cover both Microservices as well as Cloud Basics. For the cloud the account with the provider AWS.

## Assignment 1

- Create following Microservices for online book store.
- UserInfo Service, which will provide basic details about the user.
- Books Catalogue Service, which will keep track of all the books in the store.
- Order Service, which will keep track of all the store orders.
- Make use of Lambda function for creating an order.

## Assignment 2

- Create following Microservices for currency conversion based on the exchange.
- Forex Service, which will provide currency exchange values for various currency.
- Currency Conversion Service, which can convert a bucket of currencies into another currency. It uses Forex Service to get the current currency exchange values.
- Make use of Lambda function for the conversion calculation.

## Postman Collection

To use the latest published version, click the following button to import the aws-microservices-assignment API as a collection:

[![Run in Postman](https://s3.amazonaws.com/postman-static/run-button.png)](https://documenter.getpostman.com/view/16677154/TzsWtAH6)

## License

MIT

export const fileCsv = `year,industry_code_ANZSIC,industry_name_ANZSIC,rme_size_grp,variable,value,unit
2011,A,"Agriculture, Forestry and Fishing",a_0,Activity unit,46134,COUNT
2011,A,"Agriculture, Forestry and Fishing",a_0,Rolling mean employees,0,COUNT
2011,F,Wholesale Trade,d_10-19,Total expenditure,7951,DOLLARS(millions)
2011,F,Wholesale Trade,d_10-19,Operating profit before tax,391,DOLLARS(millions)
2011,F,Wholesale Trade,d_10-19,Total assets,3524,DOLLARS(millions)
2011,F,Wholesale Trade,d_10-19,Fixed tangible assets,286,DOLLARS(millions)
2011,F,Wholesale Trade,e_20-49,Activity unit,561,COUNT`;

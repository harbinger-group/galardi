import AWS from 'aws-sdk';
import { pdfBase64 } from './base64';
import fs from 'fs';
import { config } from './config';
import middleware from '../lib/middleware';

/**
 * get Json
 *
 * @param {object} event containg all the information about the event execution ex: body, headers
 * @param {object} context containing some meta data of app
 * @return {array} books array in response
 */

AWS.config.update({
  accessKeyId: config.awsAccesskeyID,
  secretAccessKey: config.awsSecretAccessKey,
  region: config.awsRegion,
});

let writeStream = fs.createWriteStream('./resultJson/dwdrink2.json');

async function getJson(event, context, token) {
  let storeJsonfile = [];
  var textract = new AWS.Textract({ apiVersion: '2018-06-27' });
  var params = {
    JobId: 'c5ab2ec5aa953cd4f1bf4ad30eba2615d19ec634504de13d9201d2f3e20fc81b' /* required */,
    MaxResults: 1000,
    NextToken: token,
  };
  const resData = await textract.getDocumentAnalysis(params).promise();

  // pagination process
  token = resData.NextToken;
  if (token == undefined) {
    console.log('not found more pages');
  } else {
    getJson(event, context, token);

    //json data store
    storeJsonfile.push(resData);
    writeStream.write(JSON.stringify(storeJsonfile));
    writeStream.on('finish', () => {
      console.log('wrote all data to file');
    });
    return {
      statusCode: 200,
      body: JSON.stringify(storeJsonfile),
    };
  }
}
export const handler = middleware(getJson);

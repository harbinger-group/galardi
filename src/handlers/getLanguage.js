import AWS from 'aws-sdk';
import { config } from './config';
const textractHelper = require('aws-textract-helper');
import middleware from '../lib/middleware';
import { handleSuccess, handleError } from 'response-helper';
/**
 * get all Language
 *
 * @param {object} event containg all the information about the event execution ex: body, headers
 * @param {object} context containing some meta data of app
 * @return {array} books array in response
 */

AWS.config.update({
  accessKeyId: config.awsAccesskeyID,
  secretAccessKey: config.awsSecretAccessKey,
  region: config.awsRegion,
});

export const getLanguage = async (event) => {
  let translate = new AWS.Translate();
  try {
    let params = {
      SourceLanguageCode: event.body.SourceLanguage /* required */,
      TargetLanguageCode: 'en' /* required */,
      Text: event.body.text /* required */,
    };

    const resData = await translate.translateText(params).promise();
    return handleSuccess(resData, event);
  } catch (e) {
    return handleError(e, event);
  }
};

import AWS from 'aws-sdk';
import {pdfBase64} from './base64';
import fs from "fs";
import {config} from './config';
import middleware from '../lib/middleware';

/**
 * get Json
 *
 * @param {object} event containg all the information about the event execution ex: body, headers
 * @param {object} context containing some meta data of app
 * @return {array} books array in response
 */

 AWS.config.update({
  accessKeyId: config.awsAccesskeyID,
  secretAccessKey: config.awsSecretAccessKey,
  region: config.awsRegion
});

let writeStream = fs.createWriteStream('./jsonFiles/processedJson.json');
async function getPdfToJson(event, context, token ) {
  var textract = new AWS.Textract({ apiVersion: '2018-06-27' });
 let NextToken;
 console.log("check here token", token)
  var params = {
    JobId: 'cf18ff698de1d2a9facfa16f1c418f9b5084972bd860d38fcfacaea43e609384', /* required */
    MaxResults: 1000,
    NextToken: token
  };
  textract.getDocumentTextDetection(params, function(err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else    // console.log(JSON.stringify(data.NextToken));           // successful response
   // const result = await processJson(data);
    token = JSON.stringify(data.NextToken);
   let NextToken = token
    writeStream.write(JSON.stringify(data));
    if (token == undefined){
      console.log("not found more pages")
    }else{
    NextToken = token
    console.log("else recursive ", NextToken)
    getPdfToJson(NextToken);
    }
    // the finish event is emitted when all data has been flushed from the stream
    writeStream.on("finish", () => {
      console.log("wrote all data to file");
    });
    // close the stream
    writeStream.end();
  });
  return {
    statusCode: 200,
    body: JSON.stringify(writeStream),
  };
}

export const handler = middleware(getPdfToJson);
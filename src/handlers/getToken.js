import AWS from 'aws-sdk';
import { pdfBase64 } from './base64';
import { config } from './config';
const textractHelper = require('aws-textract-helper');

import middleware from '../lib/middleware';

/**
 * get all books
 *
 * @param {object} event containg all the information about the event execution ex: body, headers
 * @param {object} context containing some meta data of app
 * @return {array} books array in response
 */
AWS.config.update({
  accessKeyId: config.awsAccesskeyID,
  secretAccessKey: config.awsSecretAccessKey,
  region: config.awsRegion,
});

async function getToken(event, context) {
  var textract = new AWS.Textract({ apiVersion: '2018-06-27' });

  var params = {
    DocumentLocation: {
      /* required */
      S3Object: {
        Bucket: 'test-demo-chat',
        Name: 'dwdrink2.pdf',
      },
    },
    FeatureTypes: [
      /* required */ 'TABLES',
      'FORMS',
      /* more items */
    ],
  };
  textract.startDocumentAnalysis(params, function (err, data) {
    if (err) console.log(err, err.stack);
    // an error occurred
    else console.log(data); // successful response
  });

  return {
    statusCode: 200,
    body: JSON.stringify('getToken'),
  };
}

export const handler = middleware(getToken);

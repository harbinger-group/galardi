import AWS from 'aws-sdk';
import {pdfBase64} from './base64';
import {config} from './config';
const textractHelper = require('aws-textract-helper')

import middleware from '../lib/middleware';

/**
 * get all books
 *
 * @param {object} event containg all the information about the event execution ex: body, headers
 * @param {object} context containing some meta data of app
 * @return {array} books array in response
 */
 AWS.config.update({
  accessKeyId: config.awsAccesskeyID,
  secretAccessKey: config.awsSecretAccessKey,
  region: config.awsRegion
});

async function hello(event, context) {
  var textract = new AWS.Textract({ apiVersion: '2018-06-27' });

  var params = {
    DocumentLocation: { /* required */
      S3Object: {
        Bucket: 'test-demo-chat',
        Name: 'dwbag3.pdf'
      }
    },
   
  };
  textract.startDocumentTextDetection(params, function(err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else     console.log(data);           // successful response
  });

  return {
    statusCode: 200,
    body: JSON.stringify('hello'),
  };
}

export const handler = middleware(hello);
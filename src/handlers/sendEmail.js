import { config } from 'config-helper';
import mimemessage from 'mimemessage';
import { fileCsv } from './fileCsv.js';
let mailContent = mimemessage.factory({
  contentType: 'multipart/mixed',
  body: [],
});

AWS.config.update({
  accessKeyId: config.awsAccesskeyID,
  secretAccessKey: config.awsSecretAccessKey,
  region: config.awsRegion,
});
const SES = new AWS.SES({
  region: 'us-east-1',
});
/**
 * @param {*} event
 * @param {*} context
 * @return {*}
 */

async function sendEmail(event, context) {
  //   const record = event.Records[0];
  //   const email = JSON.parse(record.body);
  //   const { csv } = email;
  let attachmentEntity = mimemessage.factory({
    contentType: 'text/plain',
    body: fileCsv,
  });

  attachmentEntity.header('Content-Disposition', 'attachment ;filename="chatbot-report.csv"');
  mailContent.body.push(attachmentEntity);
  mailContent.header('From', 'shubha@yopmail.com');
  mailContent.header('To', 'shubha.bundela@harbingergroup.com');
  mailContent.header('Subject', 'Chatbot Report');

  try {
    const result = await SES.sendRawEmail({
      RawMessage: { Data: mailContent.toString() },
    }).promise();
    console.log('Check result here: ', result);
  } catch (e) {
    console.log('error', e);
  }
}

export const handler = sendEmail;
